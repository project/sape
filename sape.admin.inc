<?php
/**
 * @file
 * The Sape module admin interface.
 */

/**
 * Menu callback. Generate the page with some statistic information.
 *
 * @see sape_menu()
 */
function sape_info_page() {
  $o = '';
  $path = variable_get('sape_file_path', drupal_get_path('module', 'ruwaredotcom_sape') .'/sape.php');
  $path = dirname($path);
  $links = variable_get('sape_host', $_SERVER['SERVER_NAME']) .'.links.db';
  $words = variable_get('sape_host', $_SERVER['SERVER_NAME']) .'.words.db';

  $info = file_get_contents($path .'/'. $links);
  $info = unserialize($info);

  $o .= '<h3>'. t('Info from %file (%fdt):', array('%file' => $links, '%fdt' => format_date(filemtime($path .'/'. $links), 'small'))) .'</h3>';

  $header = array(
    array('data' => t('IP')),
    array('data' => t('Banned')),
    array('data' => t('Hits')),
    array('data' => t('Generation time')),
    array('data' => t('Last visit')),
    array('data' => t('Actions'))
  );

  $rows = array();
  $dest = drupal_get_destination();
  foreach ($info['__sape_ips__'] as $ip) {
    //get ban flag
    if (drupal_is_denied('host', $ip)) $banned = TRUE;
    else $banned = FALSE;
    //get hits for ips
    if (module_exists('statistics')) {
      $stat_en = TRUE;
      $sql = "SELECT COUNT(a.uid) AS hits, SUM(a.timer) AS total, MAX(a.timestamp) AS last
      FROM {accesslog} AS a
      WHERE LOWER(a.hostname) LIKE ('%s')";
      $res = db_query($sql, $ip);
      $stat = db_fetch_array($res);
    }
    else $stat_en = FALSE;
    $actions = array(
      $banned ? l(t('unban'), "admin/user/rules/", array('query' => $dest)) : l(t('ban'), "admin/user/rules/add/$ip/host", array('query' => $dest)),
    );

    $rows[] = array(
      $ip,
      $banned ? t('Yes') : t('No'),
      $stat_en ? $stat['hits'] : t('n/a'),
      $stat_en ? format_interval(round($stat['total'] / 1000)) : t('n/a'),
      $stat_en ? ($stat['last'] ? format_date($stat['last'], 'small') .' '. t('(@time ago)', array('@time' => format_interval(time() - $stat['last']))) : t('No visits')) : t('n/a'),
      implode(' | ', $actions)
    );
  }

  $caption = t('Sape IPs');

  $o .= theme_table($header, $rows, array(), $caption);

  return $o;
}

/**
 * Form builder for the main Sape configuration page.
 *
 * @see sape_menu()
 * @ingroup forms
 */
function sape_admin_form(&$form_state) {
  $form = array();

  $form['sape_enabled'] = array(
   '#type' => 'checkbox',
   '#title' => t('Enable module'),
   '#default_value' => variable_get('sape_enabled', 0),
   '#description' => 'Enable module',
   '#required' => FALSE,
   '#weight' => -10,
  );

  $form['sape_usercode'] = array(
   '#type' => 'textfield',
   '#title' => t('User code (hash) from <b>SAPE.RU</b>'),
   '#default_value' => variable_get('sape_usercode', '00000000000000000000000000'),
   '#description' => t('Enter a user code from SAPE.RU, gives to you after <a href="http://www.sape.ru/r.b3b74f2ba7.php" target="_blank">registration</a>.'),
   '#required' => TRUE,
   '#weight' => -7,
  );

  $form['sape_blocks_count'] = array(
   '#type' => 'textfield',
   '#title' => t('Advertisement blocks count'),
   '#default_value' => variable_get('sape_blocks_count', 3),
   '#description' => t('Blocks where SAPE.RU will show the links'),
   '#required' => TRUE,
   '#weight' => -5,
  );

  $form['sape_context_enabled'] = array(
   '#type' => 'checkbox',
   '#title' => t('Enable context links for body'),
   '#default_value' => variable_get('sape_context_enabled', 1),
   '#description' => t('Enable context links'),
   '#required' => FALSE,
   '#weight' => -3,
  );

  $form['sape_file_path'] = array(
   '#type' => 'textfield',
   '#title' => t('Path to sape.php'),
   '#default_value' => variable_get('sape_file_path', drupal_get_path('module', 'ruwaredotcom_sape') .'/sape.php'),
   '#description' => 'Enter a path to the file sape.php (you can download it from  SAPE.RU)',
   '#required' => TRUE,
   '#weight' => -2,
  );

  return system_settings_form($form);
}

/**
 * Form builder for the main Sape configuration page.
 *
 * @see sape_menu()
 * @ingroup forms
 */
function sape_code_form(&$form_state) {
  $form = array();

    $form['sape_charset'] = array(
    '#type' => 'textfield',
    '#title' =>  t('Site charset'),
    '#default_value' => variable_get('sape_charset', 'UTF-8'),
    '#description' => 'Enter a site charset. Usually, it is "UTF-8" for Drupal sites . You can reference following <a href="http://www.php.net/manual/en/function.iconv.php">this link</a>.',
    '#required' => TRUE,
    '#weight' => -10,
  );

  $form['sape_multi_site'] = array(
    '#type' => 'checkbox',
    '#title' => t('Multi-site mode'),
    '#default_value' => variable_get('sape_multi_site', TRUE),
    '#description' => t('Setup the multi-site flag. It is neccessary, if you store your links.db and words.db in one folder for multiple sites.'),
    '#required' => TRUE,
    '#weight' => -9,
  );

  $form['sape_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Site host'),
    '#default_value' => variable_get('sape_host', $_SERVER['SERVER_NAME']),
    '#description' => t("Use this, when SAPE code can't correct find the site host."),
    '#required' => TRUE,
    '#weight' => -8,
  );

  $form['sape_verbose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Error reporting'),
    '#default_value' => variable_get('sape_verbose', FALSE),
    '#description' => t('Turn on if you want the module show you errors (verbose mode).'),
    '#required' => TRUE,
    '#weight' => -7,
  );

  $form['sape_force_show_code'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force show code'),
    '#default_value' => variable_get('sape_force_show_code', FALSE),
    '#description' => t('Turn on when you want the code always show.'),
    '#required' => TRUE,
    '#weight' => -6,
  );

  return system_settings_form($form);
}
